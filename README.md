# AODown

asmr.one 下载器

> 请勿滥用，合理下载

## 运行

1. `python -m main`
需要Python>=3.7
或者

2. `pdm run start`
需要Python>=3.7和pdm

3. [exe可执行下载](https://gitee.com/lzw-723/aodown/releases)

## 构建运行

> 构建产物可以在无Python环境下运行

1. 使用nuitka构建
`pdm run build`

2. main.dist文件夹为构建产物

Windows下点击其中的main.exe文件运行