import aodown.ao as ao

print("------asmr.one下载器------")
rj = input("请输入RJ号\n例如：243448、RJ01029894\n")
rj = ao.get_rj(rj)
if rj is None:
    print("RJ号错误")
    print("------下载器已退出------")
    exit()
work = ao.get_work(rj)
print("title: ", work["title"])

tracks = ao.get_tracks(rj)

print("tracks:\n序号", "\t", "type", "\t", "title")
for i in range(0, len(tracks)):
    t = tracks[i]
    print(str(i + 1), "\t", t["type"], "\t", t["title"])

print("------已过滤wav------")
i = input("请输入要下载的序号:\n提示：0代表下载全部\n")
i = int(i)
if i == 0:
    for d in tracks:
        ao.download(d, "RJ" + str(work["id"]))
elif i > len(tracks) or i < 0:
    print("检索不到序号的信息！")
else:
    ao.download(tracks[i - 1], "RJ" + str(work["id"]))
print("------下载完成------")