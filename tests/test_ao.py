import unittest
import aodown.ao as ao

class TestFetchMethods(unittest.TestCase):

    def test_work(self):
        self.assertEqual(ao.get_work(243448)["id"], 243448)

    def test_tracks(self):
        self.assertEqual(len(ao.get_tracks(243448)), 2)

    def test_rj(self):
        self.assertEqual(ao.get_rj("Rj000213"), "000213")
        self.assertEqual(ao.get_rj("000213"), "000213")

if __name__ == '__main__':
    unittest.main()